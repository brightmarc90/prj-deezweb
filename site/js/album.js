// construction de la section album infos
function albumInfoBuilder (arg) {
	// selection du conteneur des infos
	const albumCtnr = document.querySelector(".album-info div") ;
	// bloc div 1
	const divE1 = document.createElement("div");
	divE1.setAttribute("class", "text-center");
		// cover de l'album
		const imgE = document.createElement("img");
		imgE.setAttribute("src", arg.cover_medium);
		imgE.setAttribute("alt", "cover de l'album "+arg.title);
		imgE.setAttribute("width", "210");
		imgE.setAttribute("height", "210");
	divE1.appendChild(imgE);
	// fin bloc div 1
	// bloc div 2
	const divE2 = document.createElement("div");
		// titre de l'album
		const h1E = document.createElement("h1");
		h1E.setAttribute("class", "text-white");
		h1E.innerText = arg.title;
	divE2.appendChild(h1E);
		// infos durée, genre, année
		const pE = document.createElement("p");
		pE.setAttribute("class", "infos");
		pE.innerText = secondToHMS(arg.duration)+" - "+arg.genres.data[0].name+" - "+new Date(arg.release_date).getFullYear();		
	divE2.appendChild(pE);
		// l'artiste
		const pE2 = document.createElement("p")
		pE2.setAttribute("class", "artist");
		const aE = document.createElement("a");
		aE.setAttribute("href", "artiste.html?id_artist="+arg.artist.id);
		aE.setAttribute("class", "text-white");
		aE.innerText = arg.artist.name;
		pE2.appendChild(aE);
	divE2.appendChild(pE2);
		// lien vers deezer
		const aE2 = document.createElement("a");
		aE2.setAttribute("href", arg.link);
		aE2.setAttribute("class", "text-white");
		aE2.innerText = "Ecouter sur Deezer";
	divE2.appendChild(aE2);
	// fin bloc div 2

	albumCtnr.appendChild(divE1);
	albumCtnr.appendChild(divE2);
}

// construction de la section liste des titres
function trackListBuilder (arg) {
	// recuperation du corps du tableau
	const tBodyE = document.querySelector(".tracks tbody");
	// boucle de construction de chaque ligne du corps tableau
	arg.tracks.data.forEach( function(elmt, index) {
		const trE = document.createElement("tr");
			// colonne 1
			const tdE1 = document.createElement("td");
			tdE1.innerText = index+1;
		trE.appendChild(tdE1);
			// colonne 2
			const tdE2 = document.createElement("td");
			const iE2 = document.createElement("i");
			iE2.setAttribute("class", "fab fa-itunes-note");
			tdE2.appendChild(iE2);
		trE.appendChild(tdE2);
			// colonne 3
			const tdE3 = document.createElement("td");
			const aE3 = document.createElement("a");
			aE3.setAttribute("href", "track.html?id_track="+elmt.id);
			aE3.innerText = elmt.title;
			tdE3.appendChild(aE3);
		trE.appendChild(tdE3);
			// colonne 4
			const tdE4 = document.createElement("td");			
			const iE4 = document.createElement("i");
			iE4.setAttribute("class", "far fa-heart btn-heart");
			iE4.setAttribute("id", elmt.id);
			if(typeof localStorage !='undefined') { // pourvoir si l'id est dejà présent dans le localStorage
				if (elmt.id in localStorage) {
					iE4.style.color = '#12ab75';
				}
			}
			tdE4.appendChild(iE4);
		trE.appendChild(tdE4);
			// colonne 5
			const tdE5 = document.createElement("td");
			tdE5.innerText = secondToHMS(elmt.duration);
		trE.appendChild(tdE5);

		tBodyE.appendChild(trE);
	});
}

// Actions au chargement de la page
window.onload = function () {
	// recuperation de l'id de l'album
	const idAlbum = new URLSearchParams(location.search).get("id_album");
	// recupération de l'url
	if (idAlbum != "") {
		const requestUrl = `https://api.deezer.com/album/${idAlbum}`;
		// lancement de la requête et recuperation de la réponse
		fetch(requestUrl)
			.then(response=>response.json())
  			.then(result=>{// reponse de la requête  			
  			albumInfoBuilder(result); // construction de la section 1
  			trackListBuilder(result); // construction de la section 2
  			// recupération des boutons favoris de la page
			const favBtns = document.querySelectorAll(".btn-heart");
			// écouteur d'evenement sur les boutons fav
			favBtns.forEach( function(elmt, index) {
				elmt.addEventListener("click", favTrack, false);
				elmt.addEventListener("click", changeBtnClr, false);
			});
  		})		
	} else {
		alert("Oups! une erreur s'est produite pendant votre recherche\n Veuillez réessayer");
	}
}

// fonction de changement de couleur de bouton fav
function changeBtnClr (arg) {
	const element = arg.currentTarget;	
	if(typeof localStorage !='undefined') {
	    if(element.getAttribute("style") == "color: rgb(18, 171, 117);"){
	    	element.removeAttribute("style");
	    } else {
	    	element.style.color = "#12ab75";
	    }
	} else {
		alert("localStorage n'est pas supporté");
	}
}