// construction du bloc album infos
function albumInfoBuilder (arg) {
	// recuperation du conteneur album infos
	const albumCtnr = document.querySelector(".album-info>div");
	// construction du bloc de l'image de l'album
	const divE1 = document.createElement("div");
		// l'image de l'album
		const imgE1 = document.createElement("img");
		imgE1.setAttribute("src", arg.album.cover_big);
		imgE1.setAttribute("alt", "cover de l'album "+arg.album.title);
		imgE1.setAttribute("class", "album-cover");
		imgE1.setAttribute("width", "300");
		imgE1.setAttribute("height", "300");
		divE1.appendChild(imgE1);	
	// fin construction du bloc de l'image de l'album
	albumCtnr.appendChild(divE1);
	// construction du bloc des infos sur l'album
	const divE2 = document.createElement("div");
		// le titre de l'album
		const h1E = document.createElement("h1");
		h1E.innerText = arg.album.title;
		divE2.appendChild(h1E);
		// le paragraphe d'infos sur l'album
		const pE1 = document.createElement("p");
		pE1.setAttribute("class", "infos");
		const spanE1 = document.createElement("span");
		spanE1.innerText = arg.album.type+" - ";
		pE1.appendChild(spanE1);
		const timeE = document.createElement("time");
		timeE.setAttribute("datetime", arg.album.release_date);
		timeE.innerText = arg.album.release_date.substring(8, 10)+"/"+arg.album.release_date.substring(5, 7)+"/"+arg.album.release_date.substring(0, 4);
		pE1.appendChild(timeE);		
		divE2.appendChild(pE1);
		// le bloc de l'artiste
		const divE3 = document.createElement("div");
		divE3.setAttribute("class", "d-grid");
		const imgE2 = document.createElement("img");
		imgE2.setAttribute("src", arg.artist.picture_medium);
		imgE2.setAttribute("alt", "photo de l'artiste "+arg.artist.name);
		imgE2.setAttribute("width", "100");
		imgE2.setAttribute("height", "100");
		divE3.appendChild(imgE2);
		const pE2 = document.createElement("p");
		const aE = document.createElement("a");
		aE.setAttribute("href", "artiste.html?id_artist="+arg.artist.id);
		aE.innerText = arg.artist.name;
		pE2.appendChild(aE);
		divE3.appendChild(pE2);
		divE2.appendChild(divE3);		
	// fin construction du des infos sur l'album
	albumCtnr.appendChild(divE2);
}

// construction du bloc du lecteur audio
function soundHeadBuilder (arg) {
	// selection du conteneur pricinpal de la section lecteur
	const soundHeadCtnr = document.querySelector(".sound-head>div");
		// bloc infos du titre
		const divE1 = buildElmt("div", "", "d-grid");
			// bloc track
			const divE2 = buildElmt("div", "", "track");
				// titre de la chanson
				const h2E = buildElmt("h2");
				h2E.innerText = arg.title
			divE2.appendChild(h2E);
				// paragraphe d'infos sur le titre
				const pE1 = buildElmt("p", "", "track-info");
				const spanE1 = buildElmt("span");
				spanE1.innerText = secondToHMS(arg.duration)+" - ";
				pE1.appendChild(spanE1);
				const timeE1 = buildElmt("time", "", "", "", "", arg.release_date);
				timeE1.innerText = arg.release_date.substring(8, 10)+"/"+arg.release_date.substring(5, 7)+"/"+arg.release_date.substring(0, 4);
				pE1.appendChild(timeE1);
			divE2.appendChild(pE1);
		divE1.appendChild(divE2);
			// bloc boutons d'actions
			const divE3 = buildElmt("div");
				// bouton fav
				const aE1 = buildElmt("a", arg.id, "btn-heart", "#");
				if(typeof localStorage !='undefined') { // pourvoir si l'id est dejà présent dans le localStorage
					if (arg.id in localStorage) {
						aE1.style.color = '#12ab75';
					}
				}
				const iE1 = buildElmt("i", "", "fas fa-heart");
				aE1.appendChild(iE1);
			divE3.appendChild(aE1);
				// bouton ecouter sur deezer
				const aE2 = buildElmt("a", "", "", arg.link);
				aE2.innerText = "Ecouter sur Deezer";
			divE3.appendChild(aE2);
		divE1.appendChild(divE3);
		// fin bloc infos du titre
	soundHeadCtnr.appendChild(divE1);
		// bloc du lecteur audio
		const divE4 = buildElmt("div");
			// l'extrait
			const spanE2 = buildElmt("span");
			spanE2.innerText = "Extrait";
		divE4.appendChild(spanE2);
			// lecteur audio
			const audioE = buildElmt("audio", "", "w-80");
			audioE.setAttribute("controls", "");
			audioE.setAttribute("muted", "");
			const sourceE = buildElmt("source", "", "", "", arg.preview);
			sourceE.setAttribute("type", "audio/mp3");
			audioE.appendChild(sourceE);
			audioE.innerHTML += "Votre navigateur ne supporte cet élément audio";
		divE4.appendChild(audioE);
		// fin du lecteur audio
	soundHeadCtnr.appendChild(divE4);
}

// Actions au chargement de la page
window.onload = function () {
	// recuperation de l'id du track
	const idTrack = new URLSearchParams(location.search).get("id_track");
	// recupération de l'url
	if (idTrack != "") {
		const requestUrl = `https://api.deezer.com/track/${idTrack}`;
		// lancement de la requête et recuperation de la réponse
		fetch(requestUrl)
			.then(response=>response.json())
  			.then(result=>{// reponse de la requête  			  				
  			albumInfoBuilder(result); // construction de la section 1
  			soundHeadBuilder(result); // construction de la section 2
  			// recupération des boutons favoris de la page
			const favBtn = document.querySelector(".btn-heart");
			// écouter d'evenement sur les boutons fav
			favBtn.addEventListener("click", favTrack, false);
			favBtn.addEventListener("click", changeBtnClr, false);			
  		})		
	} else {
		alert("Oups! une erreur s'est produite pendant votre recherche\n Veuillez réessayer");
	}
}

// fonction de changement de couleur de bouton fav
function changeBtnClr (arg) {
	const element = arg.currentTarget;	
	if(typeof localStorage !='undefined') {
	    if(element.getAttribute("style") == "color: rgb(18, 171, 117);"){
	    	element.removeAttribute("style");
	    } else {
	    	element.style.color = "#12ab75";
	    }
	} else {
		alert("localStorage n'est pas supporté");
	}
}