// construction de l'url selon les options de recherche
function urlBySortOpt (inputVal, selectVal) {
	let url = "";
	switch (selectVal) {
		case "0": //album
			url = `https://api.deezer.com/search?q=${inputVal}&order=ALBUM_ASC&limit=80`;
			break;
		case "1": //artiste
			url = `https://api.deezer.com/search?q=${inputVal}&order=ARTIST_ASC&limit=80`;
			break;
		case "2": //musique
			url = `https://api.deezer.com/search?q=${inputVal}&order=TRACK_ASC&limit=80`;
			break;
		case "3": //populaires
			url = `https://api.deezer.com/search?q=${inputVal}&order=RANKING&limit=80`;
			break;
		case "4": //mieux notés
			url = `https://api.deezer.com/search?q=${inputVal}&order=RATING_ASC&limit=80`;
			break;
		default:
			url = `https://api.deezer.com/search?q=${inputVal}&limit=80`;
			break;
	}
	return url;
}

// fonction de construction des cards des titres
function cardBuilder (arg) {
	// recuperation du conteneur principal des cards
	const resultCtnr = document.getElementById("result-ctnr");
	// boucle de construction des cards
	arg.forEach( function(elmt, index) {
		const figureElmt = document.createElement("figure");
		// construction de l'image et ajout au parent
		const imgElmt = document.createElement("img");
		imgElmt.setAttribute("src", elmt.album.cover_medium);
		imgElmt.setAttribute("alt", "cover de l'album "+elmt.album.title);
		imgElmt.setAttribute("width", "200");
		imgElmt.setAttribute("height", "200");
		figureElmt.appendChild(imgElmt);
		// fin construction de l'image et ajout au parent	
		// construction du bloc des boutons d'action
		const divElmt = document.createElement("div");
		divElmt.setAttribute("class", "pos-relative");		
		// bouton play
		const aElmt1 = document.createElement("a");
		const iElmt1 = document.createElement("i");
		aElmt1.setAttribute("href", "track.html?id_track="+elmt.id);
		aElmt1.setAttribute("class", "btn-action btn-play");
		iElmt1.setAttribute("class", "fas fa-play");
		aElmt1.appendChild(iElmt1);
		divElmt.appendChild(aElmt1);
		// bouton disc
		const aElmt2 = document.createElement("a");
		const iElmt2 = document.createElement("i");
		aElmt2.setAttribute("href", "album.html?id_album="+elmt.album.id);
		aElmt2.setAttribute("class", "btn-action btn-disc");
		iElmt2.setAttribute("class", "fas fa-compact-disc");
		aElmt2.appendChild(iElmt2);
		divElmt.appendChild(aElmt2);
		// bouton fav
		const aElmt3 = document.createElement("a");
		const iElmt3 = document.createElement("i");
		aElmt3.setAttribute("href", "#");
		aElmt3.setAttribute("id", elmt.id)
		aElmt3.setAttribute("class", "btn-action btn-heart");
		iElmt3.setAttribute("class", "fas fa-heart");
		aElmt3.appendChild(iElmt3);
		if(typeof localStorage !='undefined') { // pourvoir si l'id est dejà présent dans le localStorage
			if (elmt.id in localStorage) {
				aElmt3.style.color = '#204dc2';
			}
		}
		divElmt.appendChild(aElmt3);
		figureElmt.appendChild(divElmt);
		// fin construction du bloc des boutons d'action

		// construction du bloc figcaption
		const figcaptionElmt = document.createElement("figcaption");
		const h3Elmt = document.createElement("h3");
		// titre de la chanson
		h3Elmt.setAttribute("class", "text-white");
		if(elmt.title.length>22){
			h3Elmt.setAttribute("title", elmt.title);
			h3Elmt.innerText = elmt.title.substring(0, 22)+"...";
		} else {
			h3Elmt.innerText = elmt.title;
		}
		figcaptionElmt.appendChild(h3Elmt);
		// durée de la chanson
		const pElmt1 = document.createElement("p");
		pElmt1.innerText = secondToHMS(elmt.duration);
		figcaptionElmt.appendChild(pElmt1);
		// artiste et album
		const pElmt2 = document.createElement("p");
		const aElmt4 = document.createElement("a");
		aElmt4.setAttribute("href", "artiste.html?id_artist="+elmt.artist.id);
		aElmt4.setAttribute("class", "text-green");
		aElmt4.innerText = elmt.artist.name;
		pElmt2.appendChild(aElmt4);
		pElmt2.innerHTML += "&nbsp;/&nbsp;"+elmt.album.title;
		figcaptionElmt.appendChild(pElmt2);
		figureElmt.appendChild(figcaptionElmt);
		// fin construction du bloc figcaption

		resultCtnr.appendChild(figureElmt);
	});	
}

// recupération du formulaire
const formEmlt = document.querySelector(".research form");

// Actions à la soumission du formulaire
formEmlt.addEventListener("submit", (event) => {
	// pour éviter l'envoi des paramètre à l'url
	event.preventDefault();
	// récupération des options de recherche
	const searchInput = document.querySelector("form input");
	//console.log(searchInput);
	const sortSelect = document.getElementById("sortSelect");	
	// recupération de l'url
	if (searchInput.value != "") {
		const requestUrl = urlBySortOpt(searchInput.value, sortSelect.value);
		// lancement de la requête et recuperation de la réponse
		fetch(requestUrl)
			.then(response=>response.json())
  			.then(results=>{// reponse de la requête  			
  			cardBuilder(results.data); // construction des cards grace au tableau de resultats
  			// recupération des boutons favoris de la page
			const favBtns = document.querySelectorAll(".btn-heart");
			// écouter d'evenement sur les boutons fav
			favBtns.forEach( function(elmt, index) {
				elmt.addEventListener("click", favTrack, false);
				elmt.addEventListener("click", changeBtnClr, false);
			});
  		})		
	} else {
		alert("Merci de préciser l'entité à rechercher");
	}
})

// fonction de changement de couleur de bouton fav
function changeBtnClr (arg) {
	const element = arg.currentTarget;	
	if(typeof localStorage !='undefined') {
	    if(element.getAttribute("style") == "color: rgb(32, 77, 194);"){
	    	element.removeAttribute("style");
	    } else {
	    	element.style.color = "#204dc2";
	    }
	} else {
		alert("localStorage n'est pas supporté");
	}
}