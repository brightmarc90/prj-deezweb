// Construction de la section identité de l'artiste
function artistIdBuild (arg) {
	// selection du conteneur des infos sur l'artiste
	const artistIdCtnr = document.querySelector(".artist-id>div>figure");
		// bloc image de l'artiste
		const divE = buildElmt("div");
		const imgE = buildElmt("img", "", "", "", arg.picture_medium, "", "Photo de l'artiste "+arg.name, "250", "250");
		divE.appendChild(imgE);
	artistIdCtnr.appendChild(divE);
		// bloc infos sur l'artiste
		const figCaptionE = buildElmt("figcaption");
			const h1E = buildElmt("h1", "", "text-white");
			h1E.innerText = arg.name;
		figCaptionE.appendChild(h1E);
			// paragraphe nombre d'album et fans
			const pE = buildElmt("p");
			pE.innerText = arg.nb_album+((arg.nb_album>1)? " albums" : " album")+" - ";
			pE.innerText += arg.nb_fan+((arg.nb_fan>1)? " fans" : " fan");
		figCaptionE.appendChild(pE);
			// bouton voir l'artiste sur deezer
			const aE = buildElmt("a", "", "text-white", arg.link);
			aE.innerText = "Voir l'artiste sur Deezer";
		figCaptionE.appendChild(aE);
	artistIdCtnr.appendChild(figCaptionE);
}

// Actions au chargement de la page
window.onload = function () {
	// recuperation de l'id de l'album
	const idArtist = new URLSearchParams(location.search).get("id_artist");
	// recupération de l'url
	if (idArtist != "") {
		const requestUrl = `https://api.deezer.com/artist/${idArtist}`;
		// lancement de la requête et recuperation de la réponse
		fetch(requestUrl)
			.then(response=>response.json())
  			.then(result=>{// reponse de la requête 
  			artistIdBuild(result); // construction de la section 1
  			//trackListBuilder(result); // construction de la section 2
  		})		
	} else {
		alert("Oups! une erreur s'est produite pendant votre recherche\n Veuillez réessayer");
	}
}