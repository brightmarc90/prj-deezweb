// construction du corps du tableau
function trackListBuild (arg) {
	// selection du corps du tableau;
	const tBody = document.querySelector("main table tbody");
		// construction d'une ligne du tableau
		const trE = buildElmt("tr");
			// colonne 1
			const tdE1 = buildElmt("td");
			const iE1 = buildElmt("i", "", "fab fa-itunes-note");
			tdE1.appendChild(iE1);
		trE.appendChild(tdE1);
			// colonne 2
			const tdE2 = buildElmt("td");
			const aE2 = buildElmt("a", "", "", "track.html?id_track="+arg.id);
			aE2.innerText = arg.title+" ("+arg.artist.name+" / "+arg.album.title+" ) ";
			tdE2.appendChild(aE2);			
		trE.appendChild(tdE2);
			// colonne 3
			const tdE3 = buildElmt("td");
			tdE3.innerText = secondToHMS(arg.duration);
		trE.appendChild(tdE3);
			// colonne 4
			const tdE4 = buildElmt("td");
			const aE3 = buildElmt("a", arg.id, "btn-heart", "#");
			aE3.setAttribute("title", "Retirer des favoris");
			const iE2 = buildElmt("i", "", "fas fa-trash-alt");
			aE3.appendChild(iE2);
			tdE4.appendChild(aE3);
		trE.appendChild(tdE4);

	tBody.appendChild(trE);
}

// Actions au chargement de la page
window.onload = function () {
	// parcours du localStorage et affichage des tracks
	if(typeof localStorage !='undefined') {
		for (var i = 0; i < localStorage.length; i++) {
	 		trackListBuild(JSON.parse(localStorage.getItem(localStorage.key(i))));
		}
		// recupération des boutons favoris de la page
		const delBtns = document.querySelectorAll(".btn-heart");
		// écouteur d'evenement sur les boutons fav
		delBtns.forEach( function(elmt, index) {
			elmt.addEventListener("click", favTrack, false);
			elmt.addEventListener("click", delTrack, false);
		});
	} else {
		alert("localStorage n'est pas supporté");
	}	
}

// suppression du track de la liste
function delTrack (arg) {
	const bntParent = arg.currentTarget.parentNode.parentNode;
	// selection du corps du tableau;
	const tBody = document.querySelector("main table tbody");
	// suppression de la ligne
	tBody.removeChild(bntParent);
}