// fonction de conversion seconde en hh:mm:ss
function secondToHMS (arg) {
	let hms = ""
	let heure = Math.floor(arg / 3600) ;	
	hms = (heure == 0)? "" : ((heure < 10)? "0"+heure+":" : heure+":");
	let min = Math.floor((arg % 3600) / 60);
	hms += (min == 0)? "00:" : ((min < 10)? "0"+min+":" : min+":");
	let sec = Math.ceil((arg % 3600) % 60);
	hms += (sec < 10)? "0"+sec : sec;
	return hms;
}

// fonction pour obtenir les paramètres de l'url
function getUrlParam (sVar) {
  return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

// fonction de construction d'un élement en un seul coup
function buildElmt (elmt, id, classes, href, src, dateTime, alt, width, height) {	
	const element = document.createElement(elmt);
	if (id) {
		element.setAttribute("id", id);
	}
	if (classes) {
		element.setAttribute("class", classes);
	}
	if (href) {
		element.setAttribute("href", href);
	}
	if (src) {
		element.setAttribute("src", src);
	}
	if (alt) {
		element.setAttribute("alt", alt);
	}
	if (width) {
		element.setAttribute("width", width);
	}
	if (height) {
		element.setAttribute("height", height);
	}
	if (dateTime) {
		element.setAttribute("dateTime", dateTime);
	}
	return element;
}

// actions au clic sur un bouton fav
function favTrack (arg) {
	arg.preventDefault();
	const element = arg.currentTarget.getAttribute("id");
	if(typeof localStorage !='undefined') {
  		if(element in localStorage) {  			
		    localStorage.removeItem(element);		  
	 	} else {
	 		const requestUrl = `https://api.deezer.com/track/${element}`;
			// lancement de la requête et recuperation de la réponse			
			fetch(requestUrl)
				.then(response=>response.json())
	  			.then(result=>{// reponse de la requête
	  			localStorage.setItem(element, JSON.stringify(result));
	  		})
	 	}
	} else {
		alert("localStorage n'est pas supporté");
	}
}